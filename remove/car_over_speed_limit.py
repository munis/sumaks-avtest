# -*- coding: utf-8 -*-
import tornado.web
from tornado import gen
import json
# tural
import logging

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level=logging.DEBUG)


class OvSpeedLimitHandler(tornado.web.RequestHandler):

    # @tornado.web.authenticated
    '''
    {
      "over_speed_limits": [
        {
          "car_id": 1,
          "max_latitude": 1,
          "max_value": 1,
          "max_longitude": 1,
          "id": 1,
          "longitude": 1,
          "latitude": 1
        },
        {
          "car_id": 1,
          "max_latitude": 1,
          "max_value": 1,
          "max_longitude": 1,
          "id": 2,
          "longitude": 1,
          "latitude": 1
        }
      ]
    }
    '''
    @gen.coroutine
    def get(self, car_id=None):
        try:
            # user_id = self.get_arguments("user_id")
            # car_id = self.get_arguments("car_id")
            s1 = "SELECT * FROM over_speed_limits where car_id = %s "
            cursor = yield self.application.db.execute(s1, (car_id,))
            rows = [x for x in cursor]
            cols = [x[0] for x in cursor.description]
            results = []
            for row in rows:
                result = {}
                for prop, val in zip(cols, row):
                    result[prop] = val
                results.append(result)

            if not results:
                logging.debug("over_speed_limits list is empty")
                self.set_status(404)
            else:
                self.set_header('Content-type', 'application/json')
                self.write(json.dumps({'over_speed_limits': results}))
        except Exception:
            self.set_status(400)
        finally:
            self.finish()
