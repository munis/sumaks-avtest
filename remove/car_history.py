# -*- coding: utf-8 -*-
import tornado.web
from tornado import gen
import json
# tural
import logging

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level=logging.DEBUG)


class historyRouteCarHandler(tornado.web.RequestHandler):

    @gen.coroutine
    def get(self, car_id=None):
        try:

            s1 = "SELECT ch1.longitude, ch1.latutude,ch2.longitude,ch2.latutude\
            FROM car_route_history AS ch1\
            INNER JOIN car_route_history AS ch2\
            ON ch1.id=ch2.root_id\
            WHERE ch1.car_id=%s"

            cursor = yield self.application.db.execute(s1, (car_id,))
            rows = [x for x in cursor]
            cols = [x[0] for x in cursor.description]
            results = []
            for row in rows:
                result = {}
                for prop, val in zip(cols, row):
                    result[prop] = val
                results.append(result)
            cursor.query
            cursor.statusmessage

            if not results:
                logging.debug("car history list is empty")
                self.set_status(404)
            else:
                self.set_header('Content-type', 'application/json')
                self.write(json.dumps({'histories': results}))

        except Exception:
            self.set_status(400)
        finally:
            self.finish()
